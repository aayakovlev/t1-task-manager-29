package ru.t1.aayakovlev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.FormatUtil.format;

public final class SystemShowInfoCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-i";

    @NotNull
    public static final String DESCRIPTION = "Show hardware info.";

    @NotNull
    public static final String NAME = "info";

    @Override
    @NotNull
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        @NotNull final Runtime runtime = Runtime.getRuntime();
        final long availableCores = runtime.availableProcessors();
        final long maxMemory = runtime.maxMemory();
        final long freeMemory = runtime.freeMemory();
        final long totalMemory = runtime.totalMemory();
        final long usedMemory = totalMemory - freeMemory;

        final boolean checkMaxMemory = maxMemory == Long.MAX_VALUE;
        @NotNull final String maxMemoryFormat = checkMaxMemory ? "no limit" : format(maxMemory);
        @NotNull final String freeMemoryFormat = format(freeMemory);
        @NotNull final String totalMemoryFormat = format(totalMemory);
        @NotNull final String usedMemoryFormat = format(usedMemory);

        System.out.println("[INFO]");
        System.out.println("Available cores: " + availableCores);
        System.out.println("Max memory: " + maxMemoryFormat);
        System.out.println("Total memory: " + totalMemoryFormat);
        System.out.println("Free memory: " + freeMemoryFormat);
        System.out.println("Used memory: " + usedMemoryFormat);
    }

}
