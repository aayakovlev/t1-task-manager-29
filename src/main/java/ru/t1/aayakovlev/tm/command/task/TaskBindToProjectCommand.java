package ru.t1.aayakovlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Bind task to project.";

    @NotNull
    public static final String NAME = "task-bind-to-project";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.print("Enter project id: ");
        @NotNull final String projectId = nextLine();
        System.out.print("Enter task id: ");
        @NotNull final String taskId = nextLine();
        @NotNull final String userId = getUserId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
    }

}
