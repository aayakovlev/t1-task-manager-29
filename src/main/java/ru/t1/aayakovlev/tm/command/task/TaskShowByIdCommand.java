package ru.t1.aayakovlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.model.Task;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Show task by id.";

    public static final String NAME = "task-show-by-id";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASK BY ID]");
        System.out.print("Enter id: ");
        @NotNull final String id = nextLine();
        @NotNull final String userId = getUserId();
        @Nullable final Task task = getTaskService().findById(userId, id);
        showTask(task);
    }

}
